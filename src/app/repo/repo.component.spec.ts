import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BusyModule } from 'angular2-busy';
import { HttpModule } from '@angular/http';
import { RepoComponent } from './repo.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SearchPipePipe } from '../shared/search-pipe.pipe';
import { Observable } from 'rxjs';

describe('RepoComponent', () => {
  let component: RepoComponent;
  let fixture: ComponentFixture<RepoComponent>;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  }
  let mockARouter = {
    params: Observable.of({ id: 123 })
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, FlexLayoutModule, BusyModule, HttpModule],
      declarations: [RepoComponent, SearchPipePipe],
      providers: [
        { provide: Router, useValue: mockRouter },
        { provide: ActivatedRoute, useValue: mockARouter }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

});
