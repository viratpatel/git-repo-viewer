import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Subscription } from 'rxjs';

import { SharedService } from "../shared/shared.service";
import { UserData } from '../model-data/user';
import { ReposData } from '../model-data/repos';
import { RepoData } from '../model-data/repo';
import { FollowData } from '../model-data/follower'


@Component({
  selector: 'app-repo',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.scss'],
  providers: [ SharedService ]
})
export class RepoComponent implements OnInit {

  busy: Subscription;
  listText: string = '';
  userText: string = 'mralexgray';
  repoText: string = '';
  filterText: string = '';
  user: UserData;
  repos: ReposData[];
  repo: RepoData;
  followers : FollowData[];

  constructor(private _sharedService: SharedService, private router: Router,  private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      if (params['userid'] !== undefined && params['repoid'] === undefined) {
        this.userText = params['userid'];
        this.loadUserInfo();
      } else if (params['userid'] !== undefined && params['repoid'] !== undefined) {
        this.userText = params['userid'];
        this.repoText = params['repoid'];
        this.loadRepoInfo();
      }
    });
  }

  loadUserInfo() {
    this.busy = this._sharedService.getUserInformation(this.userText).subscribe(
      user => {
        this.user = user;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  loadRepoInfo() {
    this.busy = this._sharedService.getRepoInformation(this.userText, this.repoText).subscribe(
      repo => {
        this.repo = repo;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  loadFollower() {
    this.repos = null;
    this.busy = this._sharedService.getFollowers(this.userText).subscribe(
      followers => {
        this.listText = "List of followers";
        this.followers = followers;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  loadFollowing() {
    this.repos = null;
    this.busy = this._sharedService.getFollowings(this.userText).subscribe(
      followers => {
        this.listText = "List of followings";
        this.followers = followers;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  loadRepos() {
    this.followers = null;
    this.busy = this._sharedService.getRepos(this.userText).subscribe(
      repos => {
        this.listText = "List of repository";
        this.repos = repos;
      },
      (err: any) => {
        console.log(err);
      }
    );

  }

  navigateUser(userTxt) {
    const link = ['/user', userTxt];
    this.followers = null;
    this.router.navigate(link);
  }

  navigateRepo(repoTxt) {
    const link = ['/user', this.userText, repoTxt];
    this.repos = null;
    this.router.navigate(link);
  }
}
