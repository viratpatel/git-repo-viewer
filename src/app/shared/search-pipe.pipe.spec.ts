import { SearchPipePipe } from './search-pipe.pipe';

describe('SearchPipePipe', () => {
  let pipe: SearchPipePipe;

  beforeEach(() => {
    pipe = new SearchPipePipe();
  });
  it('create an instance', () => {
    const pipe = new SearchPipePipe();
    expect(pipe).toBeTruthy();
  });
  
  it('filter repository from repositories', () => {
    let value: any[] = [ { name: 'textabctext'}, { name: 'testabctest'}];
    let args: string = 'abc';

    expect(pipe.transform(value, args)).toBeTruthy();
  })
});
