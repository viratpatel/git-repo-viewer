import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPipe'
})
export class SearchPipePipe implements PipeTransform {

  transform(value: any, args: string): any {
    return value.filter(item => item.name.toLowerCase().indexOf(args.toLowerCase()) !== -1);
  }

}
