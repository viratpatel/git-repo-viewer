import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'; 

import { UserData } from '../model-data/user';
import { RepoData } from '../model-data/repo';
import { ReposData } from '../model-data/repos';
import { FollowData } from '../model-data/follower'

@Injectable()
export class SharedService {

  base_url_user: string = 'https://api.github.com/users/'
  base_url_repo: string = 'https://api.github.com/repos/'

  constructor(private _http: Http) {
  }

  getUserInformation(userTxt: string) {
    let url = `${this.base_url_user}${userTxt}`;
    return this._http.get(url)
      .map(response => {
        return response.json();
      })
      .map(data => {
        return new UserData(data.id, data.avatar_url, data.company, data.location, data.name, data.login, data.blog);
      })
  }

  getRepoInformation(userTxt: string, repoTxt: string){
    let url = `${this.base_url_repo}${userTxt}/${repoTxt}`;
    return this._http.get(url)
      .map(response => {
        return response.json();
      })
      .map(data => {
        return new RepoData(data.owner.avatar_url, data.name, data.owner.login, data.full_name);
      });
  }

  getFollowers(userTxt: string) {
    let url = `${this.base_url_user}${userTxt}/followers`;
    return this._http.get(url)
      .map(response => {
        return response.json();
      })
      .map((data: Array<any>) => {
        let follows: Array<FollowData> = [];
        data.forEach((value => {
          follows.push(new FollowData(value.avatar_url,value.login))
        }))
        return follows;
      });
  }

  getFollowings(userTxt: string) {
    let url = `${this.base_url_user}${userTxt}/following`;
    return this._http.get(url)
      .map(response => {
        return response.json();
      })
      .map((data: Array<any>) => {
        let follows: Array<FollowData> = [];
        data.forEach((value => {
          follows.push(new FollowData(value.avatar_url,value.login))
        }))
        return follows;
      })
  }

  getRepos(userTxt: string) {
    let url = `${this.base_url_user}${userTxt}/repos`;
    return this._http.get(url)
      .map(response => {
        return response.json();
      })
      .map((data: Array<any>) => {
        let repos: Array<ReposData> = [];
        data.forEach((value => {
          repos.push(new ReposData(value.id, value.name))
        }))
        return repos;
      })
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
