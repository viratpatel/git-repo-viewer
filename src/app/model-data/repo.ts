export class RepoData {
    avatar_url: string;
    name: string;
    login: string;
    fullname: string;

    constructor(avatar_url: string, name: string, login: string, fullname: string) {
        this.avatar_url = avatar_url;
        this.name = name;
        this.login = login;
        this.fullname = fullname;
    }
}