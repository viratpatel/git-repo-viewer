export class UserData {
    uid: string;
    avatar_url: string;
    company: string;
    location: string;
    name: string;
    login: string;
    blog: string;

    constructor(uid: string, avatar_url: string, company: string, location: string, name: string, login: string, blog: string) {
        this.uid = uid;
        this.avatar_url = avatar_url;
        this.company = company;
        this.location = location;
        this.name = name;
        this.login = login;
        this.blog = blog;
    }
}