import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchText: string = 'mralexgray';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  searchRepo() {
    const link = ['/user', this.searchText];
    this.router.navigate(link);
  }

}
