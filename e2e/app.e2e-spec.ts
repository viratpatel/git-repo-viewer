import { GitRepoViewerPage } from './app.po';

describe('git-repo-viewer App', () => {
  let page: GitRepoViewerPage;

  beforeEach(() => {
    page = new GitRepoViewerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
